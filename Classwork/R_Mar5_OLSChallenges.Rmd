---
title: "OLS Challenges"
author: "Lauren K. Perez"
date: "3/5/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

*Start by opening the College Scorecard data and doing general cleaning, which you should be able to take most of from previous files.  We will be using the following variables: control, median debt, median family income, total cost, percent pell, percent federal loan, admission rate, SAT average, average faculty salary (avgfacsal), and the number of undergrads (ugds).  We did all of this for last class, so you should be able to copy and paste from that file.*

```{r}
library(haven)
setwd("/Users/royceyang/Desktop/ssi2/Classwork/")
CSdata <- read_dta("CollegeScorecard1415_forR.dta")

CSdata$debt_mdn[which(CSdata$debt_mdn == "NULL")] <- NA
CSdata$debt_mdn[which(CSdata$debt_mdn == "PrivacySuppressed")] <- NA
CSdata$control[which(CSdata$control == "NULL")] <- NA
CSdata$adm_rate[which(CSdata$adm_rate == "NULL")] <- NA
CSdata$preddeg[which(CSdata$preddeg == "NULL")] <- NA
CSdata$main[which(CSdata$main == "NULL")] <- NA
CSdata$costt4_a[which(CSdata$costt4_a == "NULL")] <- NA
CSdata$pctfloan[which(CSdata$pctfloan == "NULL")] <- NA
CSdata$sat_avg[which(CSdata$sat_avg == "NULL")] <- NA
CSdata$md_faminc[which(CSdata$md_faminc == "NULL")] <- NA
CSdata$pctpell[which(CSdata$pctpell == "NULL")] <- NA
CSdata$avgfacsal[which(CSdata$avgfacsal == "NULL")] <- NA
CSdata$ugds[which(CSdata$ugds == "NULL")] <- NA


#table(CSdata$debt_mdn)
CSdata$debt_mdn <- as.numeric(CSdata$debt_mdn)
CSdata$control <- as.numeric(CSdata$control)
CSdata$adm_rate <- as.numeric(CSdata$adm_rate)
CSdata$preddeg <- as.numeric(CSdata$preddeg)
CSdata$main <- as.numeric(CSdata$main)
CSdata$costt4_a <- as.numeric(CSdata$costt4_a)
CSdata$pctfloan <- as.numeric(CSdata$pctfloan)
CSdata$sat_avg <- as.numeric(CSdata$sat_avg)
CSdata$md_faminc <- as.numeric(CSdata$md_faminc)
CSdata$pctpell <- as.numeric(CSdata$pctpell)
CSdata$ugds <- as.numeric(CSdata$ugds)
CSdata$avgfacsal <- as.numeric(CSdata$avgfacsal)
```

*Run a multivariate regression that uses median family income, total cost, percent of students w/ Pell grants, and the percent of students with federal loans to predict median debt.  We did this February 26.*

```{r}
mod1 = lm(debt_mdn ~ md_faminc + costt4_a + pctpell + pctfloan, CSdata)
summary(mod1)
```

*Run a multivariate model, using cost as the dependent variable, and private, SAT average, admission rate, average faculty salaries, and the number of undergradates as independent variables.  We did this February 28. (Use the additive version, rather than the one with the interaction term.*

```{r}
mod2 = lm(co)
```

# Influential Cases

To check for influential cases in the median debt model, we can start with a leverage vs. residuals plot. 

*Run the code below.  Remember to change "mod7" to whatever you named your median debt model.*

```{r}
plot(mod7, which=5)
```
Looking at the graph, we can barely see the lines for Cook's distance.  We can see three dashes of the dashed lines that represent Cook's distance in the bottom left of the graph, but cannot see the rest of the lines.  This means that none of our observations are problematically influential, at least according to Cook's distance. 

If we were concerned just about leverage, observation 5791 might be the one to look into further or consider making a dummy variable for. 

*Create a dummy variable for that observation and try running the model again, including the dummy variable.  Compare the two models. How would you interpret the dummy for that observation?*

*Now do the same thing for the cost model.*

# Variance Inflation Factors (VIFs)

To check for multicollinearity, we can use the VIF score. 

*Do this for the median debt model.  Remember to change your model name.*

```{r}
library(car)
vif(mod7)
```
All of these are below 4, so we do not have a great reason for concern. 

*Now do the same thing for the cost model.*
