---
title: "Feb 5 SSI II - Bivariate Hypothesis Tests"
author: "Lauren K. Perez"
date: "2/5/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
# knitr::opts_knit$set(root.dir = "your/file/path") #Either add your working directory here or comment this out
library(haven)
```

#Data Cleaning

*Use the R chunk below to open the College Scorecard data and clean the necessary variables (turn NULL values to NA, make numeric, etc.). You should do this for the following variables: `debt_mdn`, `control`, `adm_rate`, `preddeg`, `main`. I am opening the data as CSdata (so if you open it as something else, you will need to change the code I give you to reflect that). If you've made a cleaning script, you can just copy and paste from that.*

*Hint: If you are having trouble with the as.numeric() function, especially for debt_mdn, use the table() function to see what other values are not numeric and need to be fixed.*
```{r}
setwd("/Users/royceyang/Desktop/ssi2/Classwork/")
CSdata <- read_dta("CollegeScorecard1415_forR.dta")

CSdata$debt_mdn[which(CSdata$debt_mdn == "NULL")] <- NA
CSdata$debt_mdn[which(CSdata$debt_mdn == "PrivacySuppressed")] <- NA
CSdata$control[which(CSdata$control == "NULL")] <- NA
CSdata$adm_rate[which(CSdata$adm_rate == "NULL")] <- NA
CSdata$preddeg[which(CSdata$preddeg == "NULL")] <- NA
CSdata$main[which(CSdata$main == "NULL")] <- NA

#table(CSdata$debt_mdn)
CSdata$debt_mdn <- as.numeric(CSdata$debt_mdn)
CSdata$control <- as.numeric(CSdata$control)
CSdata$adm_rate <- as.numeric(CSdata$adm_rate)
CSdata$preddeg <- as.numeric(CSdata$preddeg)
CSdata$main <- as.numeric(CSdata$main)
```


#Difference of Means T-test

*For what type of variables do we use a difference of means test?*

*Start by making a dummy variable for public vs. private, where public schools take a value of 1 and private schools take a value of 0.  You need to use the control variable to do so.  Remember, this variable indicates whether the college is public (coded 1), private non-profit (2), and private for-profit (3).*

```{r}
CSdata$public = ifelse(CSdata$control == 1, 1, 0)
```

Next, we can perform a difference of means test to decide whether students go into significantly more debt at private institutions than public ones. 

*What would be the null and alternative hypotheses?*

To test these hypotheses, we use the t.test() function, below. We call the t.test() function, and then give it the two groups of data we want to test - the median debt variable from the CS data set, where the groups are split by the public dummy that we created within the CS dataset, which is either equivalent to 0 or 1. 

```{r}
t.test(CSdata$debt_mdn[CSdata$public==0],
       CSdata$debt_mdn[CSdata$public==1])
```

Let's look through this output.  It tells us that it did a two sample t-test at the top.  Then it tells us what data it used. 

The next line gets to the results.  The t-statistic is 16.829, which is pretty large.  The degrees of freedom are also much larger than you'll find on most t-tables, at 3162.2.  The p-value is less than 2.2e-16, or 0.00000000000000022, which is significant at any conventional level of significance.  In other words, there is essentially 0 chance that we would get means this far apart if the two types of schools were the same/were from the same distribution.  Thus, we can reject the null hypothesis and conclude that private school students take out significantly more debt than public school students. 

The output then reminds us what the alternative hypothesis is, confirming what we said above.  

The next line is the confidence interval for the difference between the means.  The sample means we got (in the bottom line) were \$11,922.778 for private schools and \$9,525.635 for public schools.  Therefore, we found that the difference was:
```{r}
11922.778-9525.635
```

However, this difference is the difference between our sample means - so what is the difference between the population means?  The output tells us that we can be 95% confident that the difference between the two population means is somewhere between \$2117.851 and \$2676.434.

(Note: I typed the dollar signs with a backslash first to tell R to consider them dollar signs.  I do this because dollar signs are also the way that you indicate to R that you want to write an equation.  However, if you do not want to write an equation, letting it think you do will make things look funny.)

For example: look at how these look when you knit to the pdf version:
The difference is between \$2117.851 and \$2676.434 
vs. 
The difference is between $2117.851 and $2676.434

Equation notation lets you write things like $y=mx+b$, $\frac{2}{3}$ and $\sqrt{n}$ 
vs.  
Equation notation lets you write things like y=mx+b, 2/3 and sqrt(n).)

*Now try doing a t-test for median debt by non-profit (both private and public) and for-profit colleges.  You will need to create a nonprofit/forprofit dummy variable (from the control).  What would be the null and alternative hypotheses? Then perform the t-test across these two groups.  Is it significant?  What is the 95% confidence interval for the difference? How would you report these results in sentence form?*

```{r}
CSdata$forprofit <- ifelse(CSdata$control == 3, 1, 0)
t.test(CSdata$debt_mdn[CSdata$forprofit==0],
       CSdata$debt_mdn[CSdata$forprofit==1])
```

Next, let's try changing the variable we are taking the mean of, so that we are seeing if there are differences across admission rates for different types of schools. (Note: If you are running low on class time, you may want to skip ahead to chi-squared tests and come back to these two.)

*Perform a t-test for the difference of mean admission rates across public and private schools.  Is the difference significant?*
```{r}
t.test(CSdata$adm_rate[CSdata$public==0],
       CSdata$adm_rate[CSdata$public==1])
```


*Next, perform a t-test for the difference of mean admission rates across nonprofit and for-profit schools.  Is the difference significant?*
```{r}

```


#Chi-Squared Tests

*For what types of variables do we use a chi-squared test?*

The variable preddeg tells you about the predominant degree offered by each college.  0 means "not classified", 1 means "certificate", 2 means "associate", 3 means "bachelor", and 4 means "graduate".

We want to start by making a two-way frequency table.  We have made one-way (one variable) frequency tables before. For example:

```{r}
table(CSdata$preddeg)
```

This table tells us (among other things) that there are 2,102 schools that predominantly give out bachelor's degrees, but only 292 schools that give out predominantly graduate degrees.  

To make a two-way table, we input a second variable into to the function. 

```{r}
table(CSdata$preddeg, CSdata$public)
```

I think these are easier to read if the variable with more columns is across the top, so let's switch the variable order to flip this table around. 

```{r}
table(CSdata$public, CSdata$preddeg)
```


So, the top-most row and the left-most column give us are variable values for preddeg and public, respectively.  Thus, the second row is the private colleges and the third row is the public colleges.  Assuming that you do not change "not classified (0)" to missing (you could make an argument for both), the third column is for schools with a value of 1 for preddeg, or schools that predominantly give out associates degrees.  We see that there are 2730 schools that are private and predominantly give out associates degrees, while there are 613 that are public and predominantly give out associates degrees. 

*How many schools are there are give out predominantly bachelor's degrees in the private and public categories?*

The next question is whether there is a relationship between these variables.  It is kind of hard to tell without percentages in each box, but we can see that the modal predominant degree for public schools is an associate's degree, whereas it is certificates for private schools, and more private schools also give out bachelor's degrees than associate's degrees.  This suggests there may be a relationship.  We can use a chi-squared test to confirm. 

```{r}
chisq.test(CSdata$public, CSdata$preddeg, correct=FALSE)
```

We use the chisq.test() function, and put in the two variables of interest.  We also add the option "correct=FALSE", which may look counterintuitive, but this option turns off something called Yates' Correction for Continuity.  The use of this correction is debated, but either way it is meant for 2x2 comparisons when at least one cell of the table has a particularly small N (<5).  That does not apply, and the correction is not something you need to worry about for this class.  Our sample is large enough and our table has enough values that leaving it on would not actually change the results (see below), but it is probably a better habit to get into of turning it off. 

```{r}
chisq.test(CSdata$public, CSdata$preddeg)
```

Looking at the results of these, we see that the $X^2$ statistic is 668.78, with 4 degrees of freedom.  The p-value is less than 2.2e-16, meaning that it is significant at any conventional level and we can reject the null hypothesis of no relationship/same distribution across groups. 

*Now make a frequency table and perform a chi-squared test to see if there is a difference in predominant degree depending on whether a school is non-profit or for-profit.*
```{r}
table(CSdata$forprofit, CSdata$preddeg)
chisq.test(CSdata$forprofit, CSdata$preddeg)
```

*What is the Chi-squared statistic?  Is there a significant relationship between these variables?*

*Next, try doing a two-way frequency table for whether a campus is the main campus (`main`) and the predominant degree they offer.  Then do the chi-squared statistic.  Is there a signifcant difference?*
```{r}

```

*Try replicating one of these chi-squared tests by hand, first calculating the expected frequencies for each square and then calculating the chi-squared statistic.  You can do this on paper, or for a bonus challenge, learn how to make a table in R. Assignment 1 had one in it you could start from, or you can read instructions for a few different ways to do it here: https://pandoc.org/MANUAL.html#tables*
