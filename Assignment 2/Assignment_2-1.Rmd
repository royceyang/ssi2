---
title: "Assignment 2"
date: "Due on February 4"
author: "Royce Yang"
output: 
    pdf_document:
        number_sections: false
---

# 1
**For this section, you will use simulation and plots in R to illustrate the Central Limit Theorem.**

# 1.1   2 pts
**Start by setting the seed at 100. **

```{r}
set.seed(100)
```

# 1.2   5 pts
**Write down what the CLT states. Use Latex math mode.  (Note, this should be the CLT as it relates to the mean and standard deviation, not the sum.)**

The central limit theorem states that if you have a population with mean $\mu$ and standard deviation $\sigma$ and take sufficiently large random samples from the population with replacement, then the distribution of the sample means will be approximately normally distributed.

# 1.3   5 pts
**Generate a "population" from which you will get the samples. The population should have a chi-squared distribution with 7 degrees of freedom. (Hint: use the rchisq() function.) Your N should be 8,000,000. Present a histogram, estimate and report the population mean $\mu$ and the standard deviation $\sigma$. **

```{r}
N = 8000000
df = 7
population = rchisq(N, df)

hist(population)
mu = mean(population)
sigma = sd(population)

mu
sigma
```

# 1.4   2 pts
**Take one sample of size $n = 50$ and obtain the mean. Recall that this is the mean of a sample size $n$, and this is the random variable that should follow a normal distribution.**

```{r}
samp = sample(population, 50)
samp_mu = mean(samp)

samp_mu
```

# 1.5   5 pts
**What you have created is one realization of the variable, but to illustrate the CLT you must show a distribution.  Thus, create a vector of size 1000, and repeat the sampling exercise, saving the mean of each sample in the vector (the mean of sample 1 will be stored in the first element of the vector, the mean of sample 2 will be stored on the second element of the vector, and so on).**

```{r}
samp_mus = vector(mode="double", length=1000)
for (i in 1:1000){
  samp = sample(population, 50)
  samp_mu = mean(samp)
  samp_mus[i] = samp_mu
}

samp_mus
```

# 1.6   5 pts
**The vector you created should contain the means of 1000 samples. Now estimate its mean and standard deviation, and compare them to the theoretical distribution posited by the CLT. Are they alike?**

```{r}
samp_mus_mean = mean(samp_mus)
samp_mus_sigma = sd(samp_mus)

samp_mus_mean
samp_mus_sigma
```

```{r}
expected_clt_mean = mu
expected_clt_sd = sigma/50^0.5

expected_clt_mean
expected_clt_sd
```

Comparing the mean here to our variable expected_clt_mean, it is very close. The standard deviation compared to our variable expected_clt_sd is also very close.

# 1.7   5 pts
**Make a histogram of the mean of sample means, and use the `curve` function to compare its distribution to the theoretical distribution proposed by the CLT. Is it normally shaped?**

```{r}
hist(samp_mus, freq = F)
curve(dnorm(x, mean = expected_clt_mean, sd = expected_clt_sd), col = "blue", add = TRUE)
```

Yes, it is normally shaped.

# 1.8   5 pts
**Present a similar histogram as above but increase $n$ to 1000. Does it look more or less like a normal distribution? (You may want to set the X axes to be the same to ease comparison.) Does it follow the theoretical distribution posited by the CLT? Provide evidence towards your answer.**

```{r}
samp_mus = vector(mode="double", length=1000)
for (i in 1:1000){
  samp = sample(population, 1000)
  samp_mu = mean(samp)
  samp_mus[i] = samp_mu
}

expected_clt_mean = mu
expected_clt_sd = sigma/1000^0.5

hist(samp_mus, freq = F)
curve(dnorm(x, mean = expected_clt_mean, sd = expected_clt_sd), col = "blue", add = TRUE)
```

It looks more like a normal distribution because the two peaks in the middle are now higher and closer to the peak of the curve than before. Yes, it follows the theoretical distribution posited by the CLT because we see that the theoretical curve passes through (about) the midpoint of the top line of the bars in our histogram, which tells us it is a very good fit.

# 2
**The histogram below depicts the frequency distribution for the unemployment rate for the regions in the UK.  The data are provided by Koch et. al.**

\begin{center}
\includegraphics[width=5in]{hist_unempl_rate.pdf}
\end{center}

## 2.1  4 pts
**Do you think that the unemployment rate data are distributed normally?  Why or why not?**

No, the unemployment rate data are not distributed normally. It is skewed to the right. In other words, it is not symmetrical about the mean.

## 2.2  6 pts
**Regardless of your answer above, if we took an infinite number of regional samples size 40 of the unemployment rate, would the sampling distribution of the means be distributed normally?  Why or why not?**

No, it is mostly likely that even with infinite number of regional samples size 40 of the unemployment rate, the histogram will remain skewed to the right and therefore not symmetrical about the mean. Having more samples does not change the skewed-ness, with exception of the case when skewedness is a only result of extreme measurement error in smaller samples.

# 3

**Two other variables in the Koch et. al. data are age and how likely one is to vote in the next general election (likelyvote).  Their summary statistics are in the table below.**  

+-----------------+--------+-----------+------------+-------+--------+
| Variable        | Obs(N) | Mean      | Std. Dev   | Min   | Max    |
+=================+========+===========+============+=======+========+
| age             | 7782   | 45.43     | 14.71      | 18    | 105    |
+-----------------+--------+-----------+------------+-------+--------+
| likelyvote      | 7692   | 8.246     | 2.993      | 0     | 10     |
+-----------------+--------+-----------+------------+-------+--------+

## 3.1   5 pts
**Using the statistics in the table, calculate the standard error for the mean age. Show your work and do this without R functions.**

```{r}
std_error = 14.71/(7782^0.5)
std_error
```

## 3.2  5pts
**Using the statistics in the table, calculate the 95% confidence interval for age. Show your work and do this without R functions.**

```{r}
interval_lower = 45.43 - (1.96 * std_error)
interval_upper = 45.43 + (1.96 * std_error)

interval_lower
interval_upper
```

The 95% confidence interval is (45.10317, 45.75683).

# 4

**The British Election Study, conducted on a random sample of 7,793 adults nationwide, and found that 25% thought Blair was a competent prime minister, 18% thought he was incompetent, 6% were neutral, and 51% were unsure.**

## 4.1   3 pts
**What is the sample mean of those who thought Blair was competent (as opposed to any of the other options combined)?**

The sample mean is 0.25.

## 4.2   4 pts
**What is the sample standard deviation of Blair's competence?  Show your work.**

```{r}
standard_deviation = (1 - 0) * (0.25 * 0.75)^0.5

standard_deviation
```

## 4.3   4 pts
**What is the standard error of the mean?  Show your work.**

```{r}
standard_error = standard_deviation/(7793^0.5)
standard_error
```

## 4.4   4 pts
**What is the 95% confidence interval?  Show your work and then write the answer in sentence form.**

```{r}
interval_lower = 0.25 - (1.96 * standard_error)
interval_upper = 0.25 + (1.96 * standard_error)

interval_lower
interval_upper
```

We are 95% confident that the true value of the proportion of those who think Blair is competent is between 24.04% and 25.96% of nationwide adults.

## 4.5   3 pts
**How would you phrase this in terms of the margin of error?**

The proportion of nationwide adults who believe Blair is competent is 25%, with a (95%) margin of error of 0.96 points.

## 4.6   4 pts
**What is the 99% confidence interval?  Show your work and then write the answer in sentence form.**

```{r}
interval_lower = 0.25 - (2.575 * standard_error)
interval_upper = 0.25 + (2.575 * standard_error)

interval_lower
interval_upper
```

We are 99% confident that the true value of the proportion of those who think Blair is competent is between 23.74% and 26.26% of nationwide adults.

## 4.7   3 pts
**How would you phrase this in terms of the margin of error?** 

The proportion of nationwide adults who believe Blair is competent is 25%, with a (99%) margin of error of 1.26 points.


# 5  Replication and Data Analysis
**You will continue to use the dataset `BES2005_short.dta` provided on the Canvas website. Before you begin *make sure that the data are clean, that all respondents who answered "don't know" are set to missing, and that the variables are named appropriately.* Also, create variables for female (1 if the respondent is female and 0 otherwise), age (with the age of the respondent), and white (1 if the person is white, 0 otherwise). Make sure that these variables are set to `NA` when answers to original questions (gender, year of birth, and ethnicity) are missing too.**

```{r}
library(haven)
data <- read_dta("/Users/royceyang/Desktop/ssi2/Classwork/BES2005_short.dta")

data$pre_q13[which(data$pre_q13 == 6)]  <- NA
data$pre_q23[which(data$pre_q23 == 6)]  <- NA
data$pre_q29[which(data$pre_q29 == 11)]  <- NA
data$pre_q33[which(data$pre_q33 == 4)]  <- NA
data$pre_q34[which(data$pre_q34 == 12)]  <- NA
data$pre_q50[which(data$pre_q50 > 10)]  <- NA
data$pre_q92[which(data$pre_q92 == 6)]  <- NA
data$pre_q128[which(data$pre_q128 == 5)]  <- NA
data$pre_q148[which(data$pre_q148 == 89)]  <- NA
data$pre_q163[which(data$pre_q163 == 14)]  <- NA
data$pre_q92[which(data$pre_q92 == 6)]  <- NA

data$white <- ifelse(data$pre_q174 == 1, 1, 0)

data$female <- ifelse(data$pre_q180 == 2, 1, 0)

data$age <- 2005 - (1900 + (data$pre_q148 - 1)) # assume birthday passed at time of survey

colnames(data) <- c("region", "labour_gov", "cons_gov", "party_id", "party_strength",
                    "vote_likelihood", "competent", "blair_feeling", "labour_feeling",
                    "nat_econ_eval", "britain_iraq", "politics", "yob", "edu", "marital",
                    "income", "ethnicity", "gender", "weights", colnames(data)[20],
                    colnames(data)[21], colnames(data)[22])

any(is.na(data$ethnicity))
any(is.na(data$gender))
any(is.na(data$yob))
any(is.na(data$age))
```


## 5.1   5 pts
**Using only the functions `mean` and `sd`, calculate the the margin of error and 95% confidence interval of the mean of the evaluation of the economy (variable originally coded as `pre_q92`).**

```{r}
econ_mean = mean(data$nat_econ_eval, na.rm=TRUE)
econ_sd = sd(data$nat_econ_eval, na.rm=TRUE)

standard_error = econ_sd/(sum(!is.na(data$nat_econ_eval))^0.5)
margin_error = standard_error * 1.96
margin_error

interval_lower = econ_mean - margin_error
interval_upper = econ_mean + margin_error

interval_lower
interval_upper
```
The margin of error is 0.02136128. The 95% confidence interval of the mean of the evaluation of the economy is (2.739613, 2.782336).

## 5.2   3 pts
**Use the R built-in function to calculate the confidence interval of the same variable. Compare the results.**

```{r}
t.test(data$nat_econ_eval, conf.level=0.95)
```

Our results from the built in function are really close to the confidence interval that we calculated manually.

## 5.3   3 pts  
**Create two datasets: one containing only female respondents and one containing only male respondents.**

```{r}
data_male = subset(data, female == 0)
data_female = subset(data, female == 1)
```


## 5.4   5 pts
**Calculate the 95% confidence interval of the mean of the evaluation of the economy on each of these subpopulations and report it.**

```{r}
econ_mean = mean(data_male$nat_econ_eval, na.rm=TRUE)
econ_sd = sd(data_male$nat_econ_eval, na.rm=TRUE)

standard_error = econ_sd/(sum(!is.na(data_male$nat_econ_eval))^0.5)
margin_error = standard_error * 1.96

interval_lower = econ_mean - margin_error
interval_upper = econ_mean + margin_error

interval_lower
interval_upper

econ_mean = mean(data_female$nat_econ_eval, na.rm=TRUE)
econ_sd = sd(data_female$nat_econ_eval, na.rm=TRUE)

standard_error = econ_sd/(sum(!is.na(data_female$nat_econ_eval))^0.5)
margin_error = standard_error * 1.96

interval_lower = econ_mean - margin_error
interval_upper = econ_mean + margin_error

interval_lower
interval_upper
```

We are 95% confident that the true value of the mean of the evaluation of the economy of the male subpopulation is between 2.807511 and 2.867758.

We are 95% confident that the true value of the mean of the evaluation of the economy of the female subpopulation is between 2.650512 and 2.710638.

## 5.5   5 pts
**Choose an appropriate figure to compare the distributions of evaluation of the economy between males and females. Make sure that the figure is correctly labeled and annotated. Write 2-3 sentences describing whether there are relevant differences between these two.**

```{r}
data1 = data.frame(data_male$nat_econ_eval)
data2 = data.frame(data_female$nat_econ_eval)

p1 <- hist(data_male$nat_econ_eval+0.001)
p2 <- hist(data_female$nat_econ_eval+0.001)
plot( p1, col=rgb(0,0,1,1/4), xlim=c(0,6), main = "Male vs Female Economy Evaluations",
      xlab = "Evaluation Score")
plot( p2, col=rgb(1,0,0,1/4), xlim=c(0,6), add=T)
legend(5, 1300, legend=c("Male", "Female", "Both"),
       col=c(rgb(0,0,1,1/4), col=rgb(1,0,0,1/4), rgb(207/255, 166/255, 202/255)),
       lwd=10:10, cex=0.8)
```

It looks like females tend to give lower scores in their evaluation of the economy and males tend to give (relatively) higher scores. Although there is only a slight difference (represented by the little extra color above the light purple bars) in their evaluations, we can clearly see that datapoints for female tend towards the lower end when compare to that of the male. Lastly, we note that for the most part, female and male have overlaps in the histogram (i.e. light purple color), which means that for the most part, their distribution is similar.